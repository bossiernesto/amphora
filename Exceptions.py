class AmphoraException(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

class AmphoraValidatorException(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

class BrokenAmphoraException(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

class AmphoraIdCollision(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

class AmphoraRackDupException(AmphoraException):
    def __init__(self, *args, **kwargs):
        AmphoraException.__init__(self, *args, **kwargs)