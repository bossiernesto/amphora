from unittest import TestCase
import os, imp
from amphora.amphora import Amphora,AMPHORA_STATES
from utils import amphora_utils
from amphora.amphoraRack import *

TEMP_LOOP="temploop.py"

class TestGeneratorBackend (TestCase):

    def setUp(self):
        self.backend=Amphora()
        self.temp_folder= os.path.join(os.getcwd(),"tmp/")
        self.build_block(self.buildloop,TEMP_LOOP)

    def build_block(self,func,file):
        func(self.temp_folder+file)

    def buildloop(self,file):
        self.backend.begin(tab="    ")
        self.backend.write("def loop():")
        self.backend.indent()
        self.backend.write("a=0 ")
        self.backend.write(" for i in range(1000):")
        self.backend.indent()
        self.backend.write("   a+=i")
        self.backend.dedent()
        self.backend.write("return a")
        self.backend.dedent()
        print(file)
        self.writeToFile(file,self.backend.end())

    def writeToFile(self,path,content):
        try:
            with open(path,'w+') as f:
                f.write(content)
        except IOError as e:
            raise GeneratorError(e)

    def test_duplicate_amphora(self):
        self.assertRaises(Exceptions.AmphoraRackDupException,setAmphoraId,self.backend)

    def test_loop(self):
        loop=imp.load_source('loop',self.temp_folder+TEMP_LOOP)
        self.assertEqual(loop.loop(),499500)

    def test_states(self):
        states=['Broken','New','Cracked']
        self.assertEqual(len(AMPHORA_STATES),3)
        for i in states:
            #Check if name is consistent
            self.assertEquals(AMPHORA_STATES[i].__class__.__name__.strip("Amphora"),i)

    def test_state_instances(self):
        from amphora.amphoraState import AmhporaState
        state_instances=[ v for (k,v) in AMPHORA_STATES.iteritems()]

        for instance in state_instances:
            self.assertIsInstance(instance,AmhporaState)

class GeneratorError(RuntimeError):
    pass

