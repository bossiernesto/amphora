"""
.. module:: Amphora utils
   :platform: Linux
   :synopsis: Utils functions module.
   :copyright: (c) 2013 by Ernesto Bossi.
   :license: BSD.

.. moduleauthor:: Ernesto Bossi <bossi.ernestog@gmail.com>
"""
import errno,warnings,sys
from functools import wraps
from time import gmtime,strftime

def getTimestamp():
    return strftime("%Y-%m-%d %H:%M:%S", gmtime())

def exit_with_error(error="Non Specified Error"):
    """
        Terminates amphora with an error
    """
    print error
    sys.exit(1)

def open_if_exists(filename, mode='rb'):
    """Returns a file descriptor for the filename if that file exists,
    otherwise `None`.
    """
    try:
        return open(filename, mode)
    except IOError, e:
        if e.errno not in (errno.ENOENT, errno.EISDIR):
            raise

def _reraise(cls, val, tb):
    raise cls, val, tb

# partials
try:    #python < 2.6 compatibility issue
    from functools import partial
except ImportError:
    class partial(object):
        def __init__(self, _func, *args, **kwargs):
            self._func = _func
            self._args = args
            self._kwargs = kwargs
        def __call__(self, *args, **kwargs):
            kwargs.update(self._kwargs)
            return self._func(*(self._args + args), **kwargs)

""" Decorator functions """

def deprecated(func):
    """This is a decorator which can be used to mark functions
    as deprecated. It will result in a warning being emmitted
    when the function is used."""
    @wraps(func)
    def newDeprec(*args, **kwargs):
        warnings.warn("Call to deprecated function %s." % func.__name__,
            category=DeprecationWarning)
        return func(*args, **kwargs)
    newDeprec.__name__ = func.__name__
    newDeprec.__doc__ = func.__doc__
    newDeprec.__dict__.update(func.__dict__)
    return newDeprec

def benchmark(func):
    """
    A decorator that prints the time a function takes
    to execute.
    """
    import time
    @wraps(func)
    def wrapper(*args, **kwargs):
        t = time.clock()
        res = func(*args, **kwargs)
        print func.__name__, time.clock()-t
        return res
    return wrapper

def trace(func):
    @wraps(func)
    def wrapper(*args,**kwargs):
        print "calling %s with arguments %s,%s" % (func.__name__,args,kwargs)
        func(args,kwargs)
    return wrapper


