from Exceptions import BrokenAmphoraException
from utils.singleton import Singleton

class AmhporaState(Singleton):

    def __init__(self):
        self.state_name=self.__class__.__name__.strip('Amphora')

    def Setstate(self,amphora):
        amphora.state=self

    def pour(self,amphora):
        raise NotImplementedError

class BrokenAmphora(AmhporaState):

    def pour(self,amphora):
        raise BrokenAmphoraException, "Broken Amphora %s" %amphora.id

class CrackedAmphora(AmhporaState):

    def pour(self,amphora):
        pass    #TODO: Generate a warning

class NewAmphora(AmhporaState):

    def pour(self,amphora):
        pass    #TODO: persist