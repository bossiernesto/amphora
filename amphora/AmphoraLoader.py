"""
.. module:: Amphora Loader
   :platform: Linux
   :synopsis: A simple module loader based on the PEP302 specification.
   :copyright: (c) 2013 by Ernesto Bossi.
   :license: BSD.

.. moduleauthor:: Ernesto Bossi <bossi.ernestog@gmail.com>


"""
import sys,urllib.urlparse,imp,logging

def addLoader(self,loader,modules=[]):
    sys.meta_path =[loader(*modules)]


def isUrl(url):

    parsed= urllib.urlparse.urlparse(url)

    if parsed.scheme in ("http","https"):
        return True
    elif not parsed.scheme and parsed.path == url:
        return True

    return False


class AmphoraLoader():

    def __init__(self,settings,amphora):
        self.amphora.append(amphora) #append to amphora rack
        self.settings=settings

    def get_source(self):
        pass

    def load(self,flow,amphora=None):
        pass


#TODO: should return a module from an url
class UrlLoader():

    def find_module(self,fullname):
        pass

    def load_module(self,fullname):
        code = self.get_code(fullname)
        ispkg = self.is_package(fullname)
        mod = sys.modules.setdefault(fullname, imp.new_module(fullname))
        mod.__file__ = "<%s>" % self.__class__.__name__
        mod.__loader__ = self
        if ispkg:
            mod.__path__ = []
            mod.__package__ = fullname
        else:
            mod.__package__ = fullname.rpartition('.')[0]
        exec(code, mod.__dict__)
        return mod



#Context hooks

class WarnOnImport(object):
    def __init__(self, *args):
        self.module_names = args

    def find_module(self, fullname, path=None):
        if fullname in self.module_names:
            self.path = path
            return self
        return None

    def load_module(self, name):
        if name in sys.modules:
            return sys.modules[name]
        module_info = imp.find_module(name, self.path)
        module = imp.load_module(name, *module_info)
        sys.modules[name] = module

        logging.warning("Imported deprecated module %s", name)
        return module


