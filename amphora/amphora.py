"""
.. module:: Amphora core
   :platform: Linux
   :synopsis: A simple Code Generator based on fredrik lundh code generator.
   :copyright: (c) 2013 by Ernesto Bossi.
   :license: BSD.

.. moduleauthor:: fredrik lundh, march 1998, Ernesto Bossi <bossi.ernestog@gmail.com>

# http://www.pythonware.com
# modified MidnightCoders, Febraury 2013
"""
import string
import settings as settings
from Exceptions import AmphoraException
from validator import *
from amphoraState import *
import amphoraRack
from utils.ObservableMixin import Observable,notify

AMPHORA_STATES_OBJECTS=[BrokenAmphora.getInstance(),NewAmphora.getInstance(),CrackedAmphora.getInstance()]
AMPHORA_STATES=dict((x.state_name,x) for x in AMPHORA_STATES_OBJECTS)

class Amphora(Observable):

    def __init__(self,loader=[],tab="\t"):
        self.loaders=loader
        self.code=[]
        self.begin(tab)
        self.codeline=0
        self.validator=AmphoraValidator()
        self.setState(NewAmphora.getInstance())
        amphoraRack.setAmphoraId(self)

    @notify
    def write(self,string):
        self.validator.validate(string)
        self.code.append(self.tab * self.level + string.strip().encode(settings.ENCODING))
        self.code.append("\n") # add trailing new line
        self.codeline+=1

    def setId(self,id):
        self.id=id

    def setState(self,state):
        if isinstance(state,AmhporaState):
            self.state=state

    def indent(self):
        self.level = self.level + 1

    def dedent(self):
        if self.level == 0:
            raise AmphoraException, "internal error in code generator"
        self.level = self.level - 1

    def begin(self, tab="\t"):
        self.tab = tab
        self.level = 0

    def end(self):
        return string.join(self.code, "")

    def pour(self):
        self.state.pour(self)