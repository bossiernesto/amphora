"""
.. module:: Amphora Rack
   :platform: Linux
   :synopsis: Amphora Rack Manager and id generator.
   :copyright: (c) 2013 by Ernesto Bossi.
   :license: BSD.

.. moduleauthor:: Ernesto Bossi <bossi.ernestog@gmail.com>
"""

import hashlib,Exceptions,string,random,settings
from amphora.amphora import Amphora
from utils.singleton import Singleton

def setAmphoraId(amphora):
    if not(isinstance(amphora,Amphora)):
        raise Exceptions.AmphoraException, "Passed an object different of an Amphora"
    while True: #if there's a collision, get another random id and try again
        try:
            id_gen=hashlib.md5()
            id_gen.update(getRandomString(settings.ID_RANDOMSTRINGLENGHT))
            id=id_gen.hexdigest()
            AmphoraRack.getInstance().addAmphora(id,amphora)
        except Exceptions.AmphoraIdCollision:
            continue
        break
    amphora.setId(id)   #set id to amphora

def getRandomString(lenght):
    pool=string.digits+string.letters+string.hexdigits
    return "".join(random.choice(pool) for i in xrange(lenght))

#TODO: Make it pickeable
class AmphoraRack(Singleton):
    """
    Simple wrapper of a dictionary containing all the Amphoras
    """

    def __init__(self):
        self.amhporas={}

    def addAmphora(self,id,amphora):
        if id in self.amhporas:
            raise Exceptions.AmphoraIdCollision, "Collision with id: %s on Amphora %s" % (id,amphora)
        self.amhporas[id]=amphora
        if self.has_duplicates():
            self.removeAmphora(id)
            raise Exceptions.AmphoraRackDupException, "Amphora %s is already in the Rack!" % amphora

    def has_duplicates(self):
        return len(self.amhporas) != len(set(self.amhporas.values()))

    def getAmphora(self,id):
        try:
            return self.amhporas[id]
        except KeyError,e:
            raise Exceptions.AmphoraException,e #wrap Exception

    def removeAmphora(self,id):
        try:
            self.amhporas.pop(id)
        except ValueError:
            pass

    def applyFilters(self,filters):
        return [x for x in self.amhporas if self.applyFilters(x,filters)]

    def passedFilters(self,amphora,filters):
            for filter in filters:
                if not filter.applyfilter(amphora):
                    return False
            return True

    def reset(self):
        self.amhporas.clear()





